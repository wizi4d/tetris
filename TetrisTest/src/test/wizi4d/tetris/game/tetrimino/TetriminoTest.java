package test.wizi4d.tetris.game.tetrimino;

import com.wizi4d.tetris.game.tetrimino.Tetrimino;

import junit.framework.TestCase;

public class TetriminoTest extends TestCase {
	private Tetrimino tester;
	
	protected void setUp() throws Exception {
		super.setUp();
		tester = Tetrimino.randomTetrimino();
	}
	
	public void testRandomTetrimino() {
		assertNotNull(tester);
	}
	
	public void testGetMino() {
		assertNotNull(tester.getMino(0));
		assertNotNull(tester.getMino(3));
		assertNull(tester.getMino(Tetrimino.MINOS_COUNT));
	}
	
	public void testGetColor() {
		assertNotNull(tester.getColor());
	}
}
