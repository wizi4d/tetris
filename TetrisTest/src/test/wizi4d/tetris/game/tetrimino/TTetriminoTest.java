package test.wizi4d.tetris.game.tetrimino;

import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Colors;
import com.wizi4d.tetris.game.tetrimino.TTetrimino;
import com.wizi4d.tetris.game.tetrimino.Tetrimino;

import junit.framework.TestCase;

public class TTetriminoTest extends TestCase {
	private static final float START_X = 4.0f;
	private static final float START_Y = 18.0f;
	
	private Tetrimino tester;
	
	protected void setUp() throws Exception {
		super.setUp();
		tester = new TTetrimino();
	}

	public void testTTetrimino() {
		assertEquals(Colors.PURPLE, tester.getColor());
		assertEquals(new Point2D(START_X, START_Y), tester.position);
		assertNorth();
	}
	
	public void testRotateClockwise() {
		tester.rotateClockwise();
		assertEquals(new Point2D(START_X + 1.0f, START_Y - 1.0f), tester.position);
		assertEast();
		
		tester.rotateClockwise();
		assertEquals(new Point2D(START_X, START_Y), tester.position);
		assertSouth();
		
		tester.rotateClockwise();
		assertEquals(new Point2D(START_X + 1.0f, START_Y - 1.0f), tester.position);
		assertWest();
		
		tester.rotateClockwise();
		assertEquals(new Point2D(START_X, START_Y), tester.position);
		assertNorth();
	}
	
	public void testRotateCounterClockwise() {
		tester.rotateCounterClockwise();
		assertEquals(new Point2D(START_X + 1.0f, START_Y - 1.0f), tester.position);
		assertWest();
		
		tester.rotateCounterClockwise();
		assertEquals(new Point2D(START_X, START_Y), tester.position);
		assertSouth();
		
		tester.rotateCounterClockwise();
		assertEquals(new Point2D(START_X + 1.0f, START_Y - 1.0f), tester.position);
		assertEast();
		
		tester.rotateCounterClockwise();
		assertEquals(new Point2D(START_X, START_Y), tester.position);
		assertNorth();
	}

	private void assertNorth() {
		assertEquals(new Point2D(0.0f, 0.0f), tester.getMino(0));
		assertEquals(new Point2D(1.0f, 0.0f), tester.getMino(1));
		assertEquals(new Point2D(2.0f, 0.0f), tester.getMino(2));
		assertEquals(new Point2D(1.0f, 1.0f), tester.getMino(3));
	}
	
	private void assertEast() {
		assertEquals(new Point2D(0.0f, 2.0f), tester.getMino(0));
		assertEquals(new Point2D(0.0f, 1.0f), tester.getMino(1));
		assertEquals(new Point2D(0.0f, 0.0f), tester.getMino(2));
		assertEquals(new Point2D(1.0f, 1.0f), tester.getMino(3));
	}
	
	private void assertSouth() {
		assertEquals(new Point2D(2.0f, 1.0f), tester.getMino(0));
		assertEquals(new Point2D(1.0f, 1.0f), tester.getMino(1));
		assertEquals(new Point2D(0.0f, 1.0f), tester.getMino(2));
		assertEquals(new Point2D(1.0f, 0.0f), tester.getMino(3));
	}
	
	private void assertWest() {
		assertEquals(new Point2D(1.0f, 0.0f), tester.getMino(0));
		assertEquals(new Point2D(1.0f, 1.0f), tester.getMino(1));
		assertEquals(new Point2D(1.0f, 2.0f), tester.getMino(2));
		assertEquals(new Point2D(0.0f, 1.0f), tester.getMino(3));
	}
	
}
