package test.wizi4d.tetris.game;

import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Colors;
import com.wizi4d.tetris.game.World;
import com.wizi4d.tetris.game.tetrimino.ITetrimino;
import com.wizi4d.tetris.game.tetrimino.LTetrimino;
import com.wizi4d.tetris.game.tetrimino.OTetrimino;
import com.wizi4d.tetris.game.tetrimino.TTetrimino;
import com.wizi4d.tetris.game.tetrimino.Tetrimino;

import junit.framework.TestCase;

// TODO ������� � ������ �� ������� � ���������� �������� ������� (1 - 9)
// TODO ����������� ������ ���� � ������������� ������
// TODO ������� �����
// TODO ���������� ����� � ��������� � ��������� �����
// TODO ����������� ������� dropDown
// TODO ������������ bounds � Tetrimino ��� �������� maxX � maxY

public class WorldTest extends TestCase {
	private World tester;
	
	protected void setUp() throws Exception {
		super.setUp();
		
		tester = new World();
	}
	
	public void testWorld() {
		assertNotNull(tester.currentTetrimino);
		assertNotNull(tester.nextTetrimino);
	}
	
	public void testGetCellColor() {
		assertEquals(Colors.NONE, tester.getCellColor(4, 8));
		try {
			assertEquals(Colors.NONE, tester.getCellColor(World.WIDTH, World.HEIGHT));
			fail("Expected exception");
		} catch (Exception e) {
		}
	}
	
	public void testGetCellState() {
		assertEquals(World.CellStates.EMPTY, tester.getCellState(7, 15));
		try {
			assertEquals(World.CellStates.EMPTY, tester.getCellState(World.WIDTH, World.HEIGHT));
			fail("Expected exception");
		} catch (Exception e) {
		}
	}
	
	public void testUpdate() {
		float currentTetriminoY = tester.currentTetrimino.position.y;
		tester.update(World.BASE_TICK_TIME);
		assertEquals(currentTetriminoY - 1, tester.currentTetrimino.position.y, 0.0f);
	}
	
	public void testLockDown() {
		Tetrimino currentTetrimino = tester.currentTetrimino;
		Tetrimino nextTetrimino = tester.nextTetrimino;
		
		float currentTetriminoY = currentTetrimino.position.y;
		tester.update(World.BASE_TICK_TIME * currentTetriminoY);
		
		assertSame(currentTetrimino, tester.currentTetrimino);
		tester.update(World.LOCK_DOWN_TIME);
		assertNotSame(currentTetrimino, tester.currentTetrimino);
		assertSame(nextTetrimino, tester.currentTetrimino);
		
		for (int i = 0; i < Tetrimino.MINOS_COUNT; ++i) {
			Point2D minoPosition = currentTetrimino.getMinoGlobalPosition(i);
			assertEquals(World.CellStates.LOCKED, tester.getCellState((int)minoPosition.x, (int)minoPosition.y));
		}
		
		currentTetrimino = tester.currentTetrimino;
		nextTetrimino = tester.nextTetrimino;
		currentTetriminoY = currentTetrimino.position.y;
		
		assertEquals(World.WorldStates.MOVING, tester.getState());
		while (tester.getState() == World.WorldStates.MOVING) {
			tester.update(World.BASE_TICK_TIME);
		}
		assertSame(currentTetrimino, tester.currentTetrimino);
		assertEquals(World.WorldStates.LOCKING_DOWN, tester.getState());
		tester.update(World.LOCK_DOWN_TIME);
		assertEquals(World.WorldStates.MOVING, tester.getState());
		assertNotSame(currentTetrimino, tester.currentTetrimino);
		assertSame(nextTetrimino, tester.currentTetrimino);
		
		for (int i = 0; i < Tetrimino.MINOS_COUNT; ++i) {
			Point2D minoPosition = currentTetrimino.getMinoGlobalPosition(i);
			assertEquals(World.CellStates.LOCKED, tester.getCellState((int)minoPosition.x, (int)minoPosition.y));
		}
	}
	
	public void testGameOver() {
		tester.update(World.BASE_TICK_TIME * World.HEIGHT * 20);
		assertEquals(World.WorldStates.GAME_OVER, tester.getState());
	}
	
	public void testCommands_MOVING_state() {
		assertEquals(World.WorldStates.MOVING, tester.getState());
		
		Point2D tetriminoPosition = tester.currentTetrimino.position.copy();
		tester.moveLeft();
		assertEquals(tetriminoPosition.sub(1.0f, 0.0f), tester.currentTetrimino.position);
		
		tester.moveRight();
		assertEquals(tetriminoPosition.add(1.0f, 0.0f), tester.currentTetrimino.position);
		
		Point2D minoPosition = tester.currentTetrimino.getMinoGlobalPosition(0);
		tester.rotate();
		assertFalse(minoPosition.equals(tester.currentTetrimino.getMinoGlobalPosition(0)));
		
		tester.dropDown();
		assertEquals(World.WorldStates.LOCKING_DOWN, tester.getState());
	}
	
	public void testCommands_LOCKING_DOWN_state() {
		tester.currentTetrimino = new ITetrimino();
		tester.rotate();
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		// ����� ������ ������ ��� ���������� ���������, ���� ��� ��� �����
		tester.currentTetrimino = new LTetrimino();
		tester.dropDown();
		for (int i = 0; i < 3; ++i) {
			assertEquals(World.WorldStates.LOCKING_DOWN, tester.getState());
			tester.moveLeft();
		}
		assertEquals(World.WorldStates.MOVING, tester.getState());
		
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		tester.currentTetrimino = new LTetrimino();
		tester.dropDown();
		assertEquals(World.WorldStates.LOCKING_DOWN, tester.getState());
		tester.moveRight();
		assertEquals(World.WorldStates.MOVING, tester.getState());
		
		// ����� �������� ������ ��� ���������� ���������, ���� ��� ��� �����
		tester.currentTetrimino = new TTetrimino();
		tester.dropDown();
		assertEquals(World.WorldStates.LOCKING_DOWN, tester.getState());
		tester.rotate();
		assertEquals(World.WorldStates.MOVING, tester.getState());
	}
	
	public void testCommands_GAME_OVER_state() {
		while (tester.getState() != World.WorldStates.GAME_OVER)
			tester.update(World.BASE_TICK_TIME);
		
		// ����� ������� ������������
		Tetrimino currentTetrimino = tester.currentTetrimino;
		Point2D firstMinoPosition = currentTetrimino.getMinoGlobalPosition(0);
		tester.moveLeft();
		assertEquals(firstMinoPosition, currentTetrimino.getMinoGlobalPosition(0));
		tester.moveRight();
		assertEquals(firstMinoPosition, currentTetrimino.getMinoGlobalPosition(0));
		tester.dropDown();
		assertEquals(firstMinoPosition, currentTetrimino.getMinoGlobalPosition(0));
		tester.rotate();
		assertEquals(firstMinoPosition, currentTetrimino.getMinoGlobalPosition(0));
	}
	
	public void testMoveCollisions() {
		// �������� �� ����� ����
		for (int i = 0; i < World.WIDTH + 1; ++i) {
			tester.moveLeft();
		}
		assertEquals(0.0f, tester.currentTetrimino.position.x);
		
		// �������� �� ������ ����
		for (int i = 0; i < World.WIDTH + 1; ++i) {
			tester.moveRight();
		}
		float maxOffsetX = 0;
		for (int i = 0; i < Tetrimino.MINOS_COUNT; ++i) {
			maxOffsetX = Math.max(maxOffsetX, tester.currentTetrimino.getMino(i).x);
		}
		assertEquals(World.WIDTH - 1, (int)(tester.currentTetrimino.position.x + maxOffsetX));
		
		// �������� ������
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		// ��������
		tester.currentTetrimino = new ITetrimino();
		Point2D minoPosition = tester.currentTetrimino.getMinoGlobalPosition(0);
		tester.rotate();
		assertFalse(minoPosition.equals(tester.currentTetrimino.getMinoGlobalPosition(0)));
		tester.rotate();
		tester.rotate();
		tester.rotate();
		
		tester.dropDown();
		minoPosition = tester.currentTetrimino.getMinoGlobalPosition(0);
		tester.rotate();
		assertEquals(minoPosition, tester.currentTetrimino.getMinoGlobalPosition(0));
	}
	
	public void testLineClear() {
		tester.currentTetrimino = new ITetrimino();
		for (int i = 0; i < 3; ++i) {
			tester.moveLeft();
		}
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		tester.currentTetrimino = new ITetrimino();
		tester.moveRight();
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		tester.currentTetrimino = new TTetrimino();
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		tester.currentTetrimino = new OTetrimino();
		for (int i = 0; i < 4; ++i) {
			tester.moveRight();
		}
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		assertEquals(World.CellStates.EMPTY, tester.getCellState(0, 0));
		assertEquals(World.CellStates.EMPTY, tester.getCellState(9, 1));
		assertEquals(World.CellStates.EMPTY, tester.getCellState(5, 2));
		assertEquals(World.CellStates.LOCKED, tester.getCellState(5, 0));
		assertEquals(World.CellStates.LOCKED, tester.getCellState(5, 1));
		assertEquals(World.CellStates.LOCKED, tester.getCellState(9, 0));
	}
	
	public void testDoubleLineClear() {
		for (int tetriminoCounter = 0; tetriminoCounter < 2; ++tetriminoCounter) {
			tester.currentTetrimino = new ITetrimino();
			for (int i = 0; i < 3; ++i) {
				tester.moveLeft();
			}
			tester.dropDown();
			tester.update(World.LOCK_DOWN_TIME);
		}
		
		for (int tetriminoCounter = 0; tetriminoCounter < 2; ++tetriminoCounter) {
			tester.currentTetrimino = new ITetrimino();
			tester.moveRight();
			tester.dropDown();
			tester.update(World.LOCK_DOWN_TIME);
		}
		
		tester.currentTetrimino = new TTetrimino();
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		tester.currentTetrimino = new OTetrimino();
		for (int i = 0; i < 4; ++i) {
			tester.moveRight();
		}
		tester.dropDown();
		tester.update(World.LOCK_DOWN_TIME);
		
		assertEquals(World.CellStates.EMPTY, tester.getCellState(0, 0));
		assertEquals(World.CellStates.EMPTY, tester.getCellState(9, 0));
		assertEquals(World.CellStates.EMPTY, tester.getCellState(5, 2));
		assertEquals(World.CellStates.LOCKED, tester.getCellState(5, 0));
		assertEquals(World.CellStates.LOCKED, tester.getCellState(5, 1));
	}
}
