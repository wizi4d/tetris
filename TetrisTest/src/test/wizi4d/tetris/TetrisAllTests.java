package test.wizi4d.tetris;

import junit.framework.Test;
import junit.framework.TestSuite;
import android.test.suitebuilder.TestSuiteBuilder;

// 3A - Arrange, Act, Assert

public class TetrisAllTests extends TestSuite {
    public static Test suite() {
        return new TestSuiteBuilder(TetrisAllTests.class).includeAllPackagesUnderHere().build();
    }
}