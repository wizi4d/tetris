package com.wizi4d.tetris;

import java.util.Random;

import android.os.Bundle;

import com.wizi4d.games.framework.Music;
import com.wizi4d.games.framework.Screen;
import com.wizi4d.games.framework.SoundManager;
import com.wizi4d.games.framework.android.AndroidMusic;
import com.wizi4d.games.framework.android.AndroidSoundManager;
import com.wizi4d.games.framework.android.MultiTouchHandler;
import com.wizi4d.games.framework.android.TouchHandler;
import com.wizi4d.games.framework.android.gles.GLGame;
import com.wizi4d.tetris.mainmenu.MainMenuScreen;

public class Tetris extends GLGame {
	private Music music;
	private SoundManager soundManager;
	private TouchHandler touchHandler;
	
	public static final Random random = new Random();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		touchHandler = new MultiTouchHandler(super.getGLView());
		music = new AndroidMusic(this);
		soundManager = new AndroidSoundManager(this);
	}
	
	@Override
	public Screen getStartScreen() {
		return new MainMenuScreen(this);
	}
	
	public Music getMusic() {
		return music;
	}
	
	public SoundManager getSoundManager() {
		return soundManager;
	}
	
	public TouchHandler getTouchHandler() {
		return touchHandler;
	}
}
