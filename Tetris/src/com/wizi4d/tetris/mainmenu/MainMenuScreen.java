package com.wizi4d.tetris.mainmenu;

import java.util.List;

import com.wizi4d.games.framework.GameObject;
import com.wizi4d.games.framework.android.TouchHandler.TouchEvent;
import com.wizi4d.games.framework.android.gles.GLCamera2D;
import com.wizi4d.games.framework.android.gles.GLGame;
import com.wizi4d.games.framework.android.gles.GLScreen;
import com.wizi4d.games.framework.math.OverlapTester;
import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Tetris;
import com.wizi4d.tetris.game.GameScreen;

public class MainMenuScreen extends GLScreen {
	final GLCamera2D camera;
	final GameObject newGameButton;
	final MainMenuAssets assets;
	Point2D touch;
	
	public MainMenuScreen(GLGame glGame) {
		super(glGame);
		camera = new GLCamera2D(glGame.getGLView(), glGame.getGLGraphics(), 10.0f, 15.0f);
		newGameButton = new GameObject(5.0f, 7.5f, 10.0f, 3.0f);
		assets = MainMenuAssets.instance(glGame);
		touch = new Point2D();
	}
	
	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = ((Tetris)glGame).getTouchHandler().getTouchEvents();
		int eventsCount = touchEvents.size();
		for (int i = 0; i < eventsCount; ++i) {
			TouchEvent event = touchEvents.get(i);
			touch.set(event.x, event.y);
			camera.touchToGameWorld(touch);
			
			if (event.type == TouchEvent.TOUCH_UP) {
				if (OverlapTester.isPointInRectangle(newGameButton.bounds, touch)) {
					//assets.click.play(1);
					glGame.setScreen(new GameScreen(glGame));
					return;
				}
			}
		}
	}
	
	@Override
	public void present(float deltaTime) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void pause() {
		assets.reload();
	}
	
	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void dispose() {
		assets.dispose();
	}
	
}
