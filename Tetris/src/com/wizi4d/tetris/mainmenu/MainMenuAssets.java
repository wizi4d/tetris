package com.wizi4d.tetris.mainmenu;

import com.wizi4d.games.framework.android.gles.GLGame;
import com.wizi4d.games.framework.android.gles.ResourceManager;

public class MainMenuAssets extends ResourceManager {
	static private MainMenuAssets instance; 
	
	static MainMenuAssets instance(GLGame glGame) {
		if (instance == null)
			instance = new MainMenuAssets(glGame);
		else
			instance.glGame = glGame;
		
		return instance;
	}
	
	private MainMenuAssets(GLGame glGame) {
		super(glGame);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void reload() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
