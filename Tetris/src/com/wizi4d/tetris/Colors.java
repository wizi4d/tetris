package com.wizi4d.tetris;

public enum Colors {
	YELLOW, LIGHT_BLUE, PURPLE, ORANGE, DARK_BLUE, GREEN, RED, NONE
}