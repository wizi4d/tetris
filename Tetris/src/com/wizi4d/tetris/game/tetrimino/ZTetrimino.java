package com.wizi4d.tetris.game.tetrimino;

import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Colors;

public class ZTetrimino extends Tetrimino {
	
	public ZTetrimino() {
		super(4.0f, 18.0f);
		
		color = Colors.RED;
		minos[0] = new Point2D(0.0f, 1.0f);
		minos[1] = new Point2D(1.0f, 1.0f);
		minos[2] = new Point2D(1.0f, 0.0f);
		minos[3] = new Point2D(2.0f, 0.0f);
	}
	
}
