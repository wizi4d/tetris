package com.wizi4d.tetris.game.tetrimino;

import com.wizi4d.games.framework.GameObject;
import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Colors;
import com.wizi4d.tetris.Tetris;

// TODO ���������� ��� �����, ��� ��������� Point2D

public abstract class Tetrimino extends GameObject {
	public static final int MINOS_COUNT = 4;
	
	protected Point2D[] minos;
	protected Colors color;
	protected Point2D[] temp;
	protected float rotateOffsetX;
	
	public Tetrimino(float x, float y) {
		super(x, y, 0.0f, 0.0f);
		
		minos = new Point2D[MINOS_COUNT];
		
		temp = new Point2D[MINOS_COUNT];
		for (int i = 0; i < MINOS_COUNT; ++i) {
			temp[i] = new Point2D();
		}
	}
	
	public Point2D getMino(int index) {
		Point2D result = null;
		if (index < minos.length)
			result = minos[index];
		
		return result;
	}
	
	public Point2D getMinoGlobalPosition(int index) {
		float minoPosX = position.x + minos[index].x;
		float minoPosY = position.y + minos[index].y;
		
		return new Point2D(minoPosX, minoPosY);
	}
	
	public Colors getColor() {
		return color;
	}
	
	public void rotateClockwise() {
		float maxX = 0;
		float maxY = 0;
		for (int i = 0; i < MINOS_COUNT; ++i) {
			temp[i].set(minos[i]);
			maxX = Math.max(maxX, minos[i].x);
			maxY = Math.max(maxY, minos[i].y);
		}
		
		for (int i = 0; i < MINOS_COUNT; ++i) {
			minos[i].x = temp[i].y;
			minos[i].y = maxX - temp[i].x;
		}
		
		float offsetX = ((maxY > maxX) ? -1.0f: 1.0f) * rotateOffsetX;
		position.add(maxX - maxY - offsetX, maxY - maxX);
	}
	
	public void rotateCounterClockwise() {
		int maxX = 0;
		int maxY = 0;
		for (int i = 0; i < MINOS_COUNT; ++i) {
			temp[i].set(minos[i]);
			maxX = Math.max(maxX, (int)minos[i].x);
			maxY = Math.max(maxY, (int)minos[i].y);
		}
		
		for (int i = 0; i < MINOS_COUNT; ++i) {
			minos[i].x = maxY - temp[i].y;
			minos[i].y = temp[i].x;
		}
		
		position.add(maxX - maxY - ((maxY > maxX) ? -1.0f: 1.0f) * rotateOffsetX, maxY - maxX);
	}
	
	public void moveLeft() {
		position.sub(1.0f, 0.0f);
	}

	public void moveRight() {
		position.add(1.0f, 0.0f);
	}
	
	public static Tetrimino randomTetrimino() {
		Tetrimino result = null;
		switch (Tetris.random.nextInt(6)) {
		case 0:
			result = new OTetrimino();
			break;
		case 1:
			result = new ITetrimino();
			break;
		case 2:
			result = new TTetrimino();
			break;
		case 3:
			result = new LTetrimino();
			break;
		case 4:
			result = new JTetrimino();
			break;
		case 5:
			result = new STetrimino();
			break;
		case 6:
			result = new ZTetrimino();
			break;
		}
		
		return result;
	}
	
}
