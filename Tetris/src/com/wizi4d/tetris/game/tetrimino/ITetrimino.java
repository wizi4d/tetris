package com.wizi4d.tetris.game.tetrimino;

import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Colors;

public class ITetrimino extends Tetrimino {
	
	public ITetrimino() {
		super(3.0f, 19.0f);
		
		color = Colors.LIGHT_BLUE;
		
		minos[0] = new Point2D(0.0f, 0.0f);
		minos[1] = new Point2D(1.0f, 0.0f);
		minos[2] = new Point2D(2.0f, 0.0f);
		minos[3] = new Point2D(3.0f, 0.0f);
		
		rotateOffsetX = 2;
	}
	
}
