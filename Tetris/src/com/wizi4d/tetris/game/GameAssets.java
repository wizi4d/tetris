package com.wizi4d.tetris.game;

import com.wizi4d.games.framework.android.gles.GLGame;
import com.wizi4d.games.framework.android.gles.ResourceManager;

public class GameAssets extends ResourceManager {
	static private GameAssets instance; 
	
	static GameAssets instance(GLGame glGame) {
		if (instance == null)
			instance = new GameAssets(glGame);
		else
			instance.glGame = glGame;
		
		return instance;
	}
	
	private GameAssets(GLGame glGame) {
		super(glGame);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void reload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
}
