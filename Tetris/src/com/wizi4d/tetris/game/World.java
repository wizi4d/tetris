package com.wizi4d.tetris.game;

import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Colors;
import com.wizi4d.tetris.game.tetrimino.Tetrimino;

public class World {
	public static final int WIDTH = 10;
	public static final int HEIGHT = 20;
	
	public static final float LOCK_DOWN_TIME = 0.5f;
	public static final float BASE_TICK_TIME = 1.5f;
	
	public enum CellStates {
		EMPTY, LOCKED
	}
	
	private class Cell {
		CellStates state;
		Colors color;
		
		Cell() {
			state = CellStates.EMPTY;
			color = Colors.NONE;
		}
	}
	
	public enum WorldStates {
		MOVING, LOCKING_DOWN, GAME_OVER
	}
	
	private final Cell[][] matrix;
	private WorldStates state;
	private float timeSinceLastUpdate;
	
	public Tetrimino currentTetrimino;
	public Tetrimino nextTetrimino;
	
	public World() {
		matrix = new Cell[WIDTH][HEIGHT];
		for (int i = 0; i < matrix.length; ++i) {
			for (int j = 0; j < matrix[i].length; ++j) {
				matrix[i][j] = new Cell();
			}
		}
		
		state = WorldStates.MOVING;
		
		currentTetrimino = Tetrimino.randomTetrimino();
		nextTetrimino = Tetrimino.randomTetrimino();
	}

	public Colors getCellColor(int x, int y) {
		return matrix[x][y].color;
	}

	public CellStates getCellState(int x, int y) {
		return matrix[x][y].state;
	}
	
	public WorldStates getState() {
		return state;
	}
	
	public void moveLeft() {
		if (!haveCollision(-1, 0) && state != WorldStates.GAME_OVER) {
			currentTetrimino.moveLeft();
			checkUnlock();
		}
	}
	
	private void checkUnlock() {
		if (state == WorldStates.LOCKING_DOWN && !haveCollision(0, -1)) {
			state = WorldStates.MOVING;
		}
	}

	public void moveRight() {
		if (!haveCollision(1, 0) && state != WorldStates.GAME_OVER) {
			currentTetrimino.moveRight();
			checkUnlock();
		}
	}

	public void rotate() {
		if (state == WorldStates.GAME_OVER)
			return;
		
		currentTetrimino.rotateClockwise();
		if (haveCollision())
			currentTetrimino.rotateCounterClockwise();
		else
			checkUnlock();
	}

	public void dropDown() {
		if (state == WorldStates.GAME_OVER)
			return;
		
		while (state != World.WorldStates.LOCKING_DOWN) {
			update(World.LOCK_DOWN_TIME);
		}
	}
	
	public void update(float deltaTime) {
		timeSinceLastUpdate += deltaTime;
		
		boolean process = true;
		while (process) {
			if ((state == WorldStates.MOVING) && (timeSinceLastUpdate >= BASE_TICK_TIME)) {
				timeSinceLastUpdate -= BASE_TICK_TIME;
				
				processMoving();
			} else if ((state == WorldStates.LOCKING_DOWN) && (timeSinceLastUpdate >= LOCK_DOWN_TIME)) {
				timeSinceLastUpdate -= LOCK_DOWN_TIME;
				
				processLockDown();
			} else {
				process = false;
			}
		}
	}
	
	private void processMoving() {
		stepDown();
		checkLockDown();
	}

	private void stepDown() {
		currentTetrimino.position.sub(0.0f, 1.0f);
	}
	
	private void checkLockDown() {
		boolean isSurfaceReached = haveCollision(0, -1);
		
		if (isSurfaceReached) {
			state = WorldStates.LOCKING_DOWN;
		}
	}
	
	private boolean haveCollision() {
		return haveCollision(0, 0);
	}
	
	private boolean haveCollision(int offsetX, int offsetY) {
		boolean haveCollision = false;
		int x, y;
		for (int i = 0; i < Tetrimino.MINOS_COUNT; ++i) {
			Point2D minoPossition = currentTetrimino.getMinoGlobalPosition(i);
			x = (int)minoPossition.x + offsetX;
			y = (int)minoPossition.y + offsetY;
			if (x < 0 || x == WIDTH || y < 0 || y == HEIGHT || getCellState(x, y) == CellStates.LOCKED) {
				haveCollision = true;
				break;
			}
		}
		
		return haveCollision;
	}
	
	private void processLockDown() {
		int affectedLinesMin = 10;
		int affectedLinesMax = -1;
		for (int i = 0; i < Tetrimino.MINOS_COUNT; ++i) {
			Point2D minoPossition = currentTetrimino.getMinoGlobalPosition(i);
			Cell cell = matrix[(int)minoPossition.x][(int)minoPossition.y];
			cell.state = CellStates.LOCKED;
			cell.color = currentTetrimino.getColor();
			affectedLinesMin = Math.min(affectedLinesMin, (int)minoPossition.y);
			affectedLinesMax = Math.max(affectedLinesMax, (int)minoPossition.y);
		}
		
		processLineClear(affectedLinesMin, affectedLinesMax);
		currentTetrimino = nextTetrimino;
		nextTetrimino = Tetrimino.randomTetrimino();
		
		if (checkGameOver()) {
			state = WorldStates.GAME_OVER;
		} else {
			state = WorldStates.MOVING;
		}
	}
	
	private void processLineClear(int affectedLinesMin, int affectedLinesMax) {
		for (int line = affectedLinesMax; line >= affectedLinesMin; --line) {
			int cellX = 0;
			boolean canClearLine = true;
			while (canClearLine && cellX < World.WIDTH) {
				canClearLine = (getCellState(cellX, line) == CellStates.LOCKED);
				++cellX;
			}
			if (canClearLine) {
				lineClear(line);
			}
		}
	}
	
	private void lineClear(int line) {
		boolean haveLockedCells;
		Cell cell;
		Cell upperCell;
		do {
			haveLockedCells = false;
			for (int cellX = 0; cellX < World.WIDTH; ++cellX) {
				if (line == World.HEIGHT - 1) {
					cell = matrix[cellX][line];
					cell.state = CellStates.EMPTY;
					cell.color = Colors.NONE;
				} else {
					cell = matrix[cellX][line];
					upperCell = matrix[cellX][line + 1];
					cell.state = upperCell.state;
					cell.color = upperCell.color;
					if (upperCell.state == CellStates.LOCKED) {
						haveLockedCells = true;
					}
				}
			}
			++line;
		} while (haveLockedCells && (line < World.HEIGHT));
	}
	
	private boolean checkGameOver() {
		boolean isGameOver = haveCollision();
		
		return isGameOver;
	}
	
}
