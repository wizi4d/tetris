package com.wizi4d.tetris.game;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.wizi4d.games.framework.GameObject;
import com.wizi4d.games.framework.android.TouchHandler.TouchEvent;
import com.wizi4d.games.framework.android.gles.GLCamera2D;
import com.wizi4d.games.framework.android.gles.GLGame;
import com.wizi4d.games.framework.android.gles.GLScreen;
import com.wizi4d.games.framework.math.OverlapTester;
import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Tetris;

public class GameScreen extends GLScreen {
	
	enum GameStates {
		READY, RUNNING, PAUSE, FINISH
	}
	
	final GLCamera2D camera;
	final GameAssets assets;
	final Point2D touch;
	final WorldRenderer worldRenderer;
	final World world;
	GameStates state;
	final GameObject moveLeftButton;
	final GameObject rotateButton;
	final GameObject moveRightButton;
	final GameObject dropDownButton;
	
	public GameScreen(GLGame glGame) {
		super(glGame);
		float cameraWidth = 10.0f;
		float cameraHeight = 12.0f;
		camera = new GLCamera2D(glGame.getGLView(), glGame.getGLGraphics(), cameraWidth, cameraHeight);
		assets = GameAssets.instance(glGame);
		touch = new Point2D();
		world = new World();
		worldRenderer = new WorldRenderer(glGame.getGLView(), glGame.getGLGraphics(), world);
		state = GameStates.READY;
		
		float upperButtonsWidth = cameraWidth / 3.0f;
		float upperButtonsHeight = cameraHeight * 0.8f;
		float loverButtonHeight = cameraHeight - upperButtonsHeight;
		moveLeftButton = new GameObject(upperButtonsWidth / 2.0f, loverButtonHeight + upperButtonsHeight / 2.0f, upperButtonsWidth, upperButtonsHeight);
		rotateButton = new GameObject(upperButtonsWidth * 1.5f, loverButtonHeight + upperButtonsHeight / 2.0f, upperButtonsWidth, upperButtonsHeight);
		moveRightButton = new GameObject(upperButtonsWidth * 2.5f, loverButtonHeight + upperButtonsHeight / 2.0f, upperButtonsWidth, upperButtonsHeight);
		dropDownButton = new GameObject(cameraWidth / 2.0f, loverButtonHeight / 2.0f, cameraWidth, loverButtonHeight);
	}
	
	@Override
	public void update(float deltaTime) {
		switch (state) {
		case READY:
			updateReady();
			break;
		case RUNNING:
			updateRunning(deltaTime);
			break;
		case PAUSE:
			break;
		case FINISH:
			break;
		}
	}

	private void updateReady() {
		if (((Tetris)glGame).getTouchHandler().getTouchEvents().size() > 0)
			state = GameStates.RUNNING;
	}
	
	private void updateRunning(float deltaTime) {
		world.update(deltaTime);
		
		List<TouchEvent> touchEvents = ((Tetris)glGame).getTouchHandler().getTouchEvents();
		int eventsCount = touchEvents.size();
		for (int i = 0; i < eventsCount; ++i) {
			TouchEvent event = touchEvents.get(i);
			touch.set(event.x, event.y);
			camera.touchToGameWorld(touch);
			
			if (event.type == TouchEvent.TOUCH_UP) {
				if (OverlapTester.isPointInRectangle(moveLeftButton.bounds, touch)) {
					world.moveLeft();
				} else if (OverlapTester.isPointInRectangle(rotateButton.bounds, touch)) {
					world.rotate();
				} else if (OverlapTester.isPointInRectangle(moveRightButton.bounds, touch)) {
					world.moveRight();
				} else if (OverlapTester.isPointInRectangle(dropDownButton.bounds, touch)) {
					world.dropDown();
				}
			}
		}
	}

	@Override
	public void present(float deltaTime) {
		GL10 gl = glGame.getGLGraphics().getGL();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		worldRenderer.render();
		
		switch (state) {
		case READY:
			presentReady(gl);
			break;
		case RUNNING:
			presentRunning(gl);
			break;
		case PAUSE:
			break;
		case FINISH:
			break;
		}
	}
	
	private void presentReady(GL10 gl) {
		// TODO Auto-generated method stub
		
	}

	private void presentRunning(GL10 gl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
}
