package com.wizi4d.tetris.game;

import javax.microedition.khronos.opengles.GL10;

import android.view.View;

import com.wizi4d.games.framework.android.gles.GLCamera2D;
import com.wizi4d.games.framework.android.gles.GLGraphics;
import com.wizi4d.games.framework.android.gles.SingleColorSquare;
import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.tetris.Colors;
import com.wizi4d.tetris.game.tetrimino.Tetrimino;

public class WorldRenderer {
	
	final private GLGraphics glGraphics;
	final private World world;
	final private GLCamera2D camera;
	final static float MINO_SIZE = 1.0f;
	
	private SingleColorSquare square;
	private float borderWidth;
	
	public WorldRenderer(View view, GLGraphics glGraphics, World world) {
		this.glGraphics = glGraphics;
		this.world = world;
		
		borderWidth = (float)(World.WIDTH) / 20;
		float aspectRatio = (float)view.getHeight() / (float)view.getWidth();
		float cameraWidth = (float)World.HEIGHT / aspectRatio;
		float cameraHeigh = (float)World.HEIGHT + borderWidth;
		camera = new GLCamera2D(view, glGraphics, cameraWidth, cameraHeigh);
		camera.position.set(cameraWidth / 2, cameraHeigh / 2);
		
		square = new SingleColorSquare();
	}
	
	public void render() {
		GL10 gl = glGraphics.getGL();
		camera.setViewportAndMatrices();
		
		renderMatrix(gl);
		renderCurrentTetrimino(gl);
		//renderNextTetrimino(gl);
	}

	private void renderMatrix(GL10 gl) {
		square.setColor(0.5f, 0.5f, 0.5f, 1.0f);
		float height = (float)World.HEIGHT;
		square.fillVertices(borderWidth / 2, height / 2 + borderWidth, borderWidth, height);
		square.render(gl);
		float width = (float)World.WIDTH;
		square.fillVertices(width / 2 + borderWidth, borderWidth / 2, width + 2 * borderWidth, borderWidth);
		square.render(gl);
		square.fillVertices(borderWidth + width + borderWidth / 2, height / 2 + borderWidth, borderWidth, height);
		square.render(gl);
		
		for (int x = 0; x < World.WIDTH; ++x) {
			for (int y = 0; y < World.HEIGHT; ++y) {
				Colors cellColor = world.getCellColor(x, y);
				setSquareColor(square, cellColor);
				square.fillVertices(borderWidth + MINO_SIZE * (float)x + MINO_SIZE / 2.0f
						,borderWidth + MINO_SIZE * (float)y + MINO_SIZE / 2.0f
						,MINO_SIZE
						,MINO_SIZE);
				square.render(gl);
			}
		}
	}
	
	private void setSquareColor(SingleColorSquare square, Colors cellColor) {
		switch (cellColor) {
		case DARK_BLUE:
			square.setColor(0.0f, 0.0f, 0.6f, 1.0f);
			break;
		case GREEN:
			square.setColor(0.0f, 1.0f, 0.0f, 1.0f);
			break;
		case LIGHT_BLUE:
			square.setColor(0.0f, 1.0f, 1.0f, 1.0f);
			break;
		case NONE:
			square.setColor(0.0f, 0.0f, 0.0f, 1.0f);
			break;
		case ORANGE:
			square.setColor(1.0f, 0.5f, 0.0f, 1.0f);
			break;
		case PURPLE:
			square.setColor(0.5f, 0.0f, 1.0f, 1.0f);
			break;
		case RED:
			square.setColor(1.0f, 0.0f, 0.0f, 1.0f);
			break;
		case YELLOW:
			square.setColor(1.0f, 1.0f, 0.0f, 1.0f);
			break;
		}
	}
	
	private void renderCurrentTetrimino(GL10 gl) {
		setSquareColor(square, world.currentTetrimino.getColor());
		for (int i = 0; i < Tetrimino.MINOS_COUNT; ++i) {
			Point2D minoPossition = world.currentTetrimino.getMinoGlobalPosition(i);
			square.fillVertices(borderWidth + MINO_SIZE * minoPossition.x + MINO_SIZE / 2.0f
					,borderWidth + MINO_SIZE * minoPossition.y + MINO_SIZE / 2.0f
					,MINO_SIZE
					,MINO_SIZE);
			square.render(gl);
		}
	}
}
